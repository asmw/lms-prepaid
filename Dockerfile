FROM python:3.8-alpine
COPY . /app
WORKDIR /app
RUN apk add --no-cache --virtual .build-deps gcc musl-dev curl git libffi-dev openssl-dev \
    && curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python \
    && /root/.poetry/bin/poetry install --no-dev \
    && apk del .build-deps gcc musl-dev curl git libffi-dev openssl-dev
CMD ["/root/.poetry/bin/poetry", "run", "lms-prepaid"]
